package StandardOutput;

import java.io.PrintStream;

public class Main {

    public static void main(String[] args) {
        PrintStream stream = new PrintStream(System.out);
        stream.println(true);
        stream.println('A');
        stream.println("ABC");
        stream.println(3.5);
        stream.println(-9.3);
        stream.println(88);
        stream.println(9000000);
        stream.println("abc");


        stream.flush();
        stream.close();

    }
}
